package Sound;

public class PlayingSound {

    private static Sound BACKGROUNDSOUND = new Sound("/src/resources/Sound/backgroundSound.wav");
    private static Sound CORRECTANSWER = new Sound("/src/resources/Sound/you_got_it_2.wav");
    private static Sound WRONGANSWER = new Sound("/src/resources/Sound/maybe-next-time-huh.wav");
    private static Sound WIN = new Sound("/src/resources/Sound/win.wav");


    //PLAYING SOUND

    public static void playBackground(){

        BACKGROUNDSOUND.play(true);
        BACKGROUNDSOUND.setLoop(1000);
    }

    public static void stopBackground(){
        BACKGROUNDSOUND.stop();
    }



    public static void playCorrectAnswer(){
        CORRECTANSWER.play(true);
    }

    public static void playWrongAnswer (){
        WRONGANSWER.play(true);
    }

    public static void playVictory(){
        WIN.play(true);
    }


}
