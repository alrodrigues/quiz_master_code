import utils.Game;
import utils.KeyboardController;


public class Main {

    public static void main(String[] args) throws InterruptedException {

        Game quiz = new Game();
        KeyboardController keyboardController = new KeyboardController(quiz);
        quiz.initGame();



    }
}
