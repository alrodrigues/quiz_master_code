package Canvas;

import org.academiadecodigo.simplegraphics.pictures.Picture;

import java.io.IOException;

public class Animations {

    int delay = 20;

    public void flashButton(Picture button){
        try {
            button.draw();
            Thread.sleep(400);
            button.delete();
            Thread.sleep(400);
        }catch (InterruptedException e){
            System.err.println(e.getMessage());
        }

    }

    public void moveTextFromLeft(Picture text){

        try {
            Thread.sleep(1);
            text.translate(1,0);
        }catch (InterruptedException e){
            System.err.println(e.getMessage());
        }
    }

    public void moveTextFromTop(Picture text){

        try {
            Thread.sleep(1);
            text.translate(0,1);
        }catch (InterruptedException e){
            System.err.println(e.getMessage());
        }
    }

}
