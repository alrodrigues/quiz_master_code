package Canvas;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Text;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Drawings {

    private Text textLives;

    public void drawBackground(String path) {
        Picture background;
        background = new Picture(10, 10, path);
        background.draw();

    }

    public void drawLives(int lives) {

        Picture starLife = new Picture(1100, 15, Imagens.STAR_LIFE);
        starLife.draw();

        textLives = new Text(1180, 38, "" + lives);
        textLives.setColor(Color.MAGENTA);
        textLives.grow(10, 20);
        textLives.draw();

    }

    public void hideTextLives() {
        this.textLives.delete();
    }

    public void drawPressSpaceEndGame() {
        Picture smallButtonPressSpace = new Picture(540, 490, Imagens.SMALL_PRESS_BUTTON);
        smallButtonPressSpace.draw();
    }

    public void drawCorrectAnswers(int correctAnswers) {

        Text endGameText;
        endGameText = new Text(620, 490, "Correct Answers " + correctAnswers);
        endGameText.grow(90, 15);
        endGameText.setColor(Color.MAGENTA);
        endGameText.draw();
    }

    public Picture drawPressSpaceStart() {
        return new Picture(535, 570, Imagens.SMALL_PRESS_BUTTON);
    }

    public void drawPressSpaceNext() {
        Picture smallButtonPressSpace = new Picture(900, 680, Imagens.SMALL_PRESS_BUTTON);
        smallButtonPressSpace.draw();
    }

    public void drawText(Picture text){
        text.draw();
    }
}

