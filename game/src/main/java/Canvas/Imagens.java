package Canvas;

import org.academiadecodigo.simplegraphics.pictures.Picture;

import javax.print.DocFlavor;

public class Imagens {

    public static final String START_BACKGROUND = "src/resources/images/background/start_background.jpg";
    public static final String QUESTIONS_BACKGROUND = "src/resources/images/background/backgroundBlueAndPurple.png";
    public static final String GAME_OVER_BACKGROUND ="src/resources/images/background/gameOver.jpg";
    public static final String WIN_BACKGROUND ="src/resources/images/background/win.jpg";
    public static final String BACKGROUND ="src/resources/images/background/backgroundAllgame.jpg";
    public static final String SMALL_PRESS_BUTTON = "src/resources/images/background/smallStartButtonText.png";
    public static final String STAR_LIFE = "src/resources/images/background/star_lifes.png";


    public static final String FIRST_QUESTION = "src/resources/images/questionsPics/FirstQuestion/FirstQuestion.png";
    public static final String FIRST_ANSWER_A = "src/resources/images/questionsPics/FirstQuestion/FirstAnswerA.png";
    public static final String FIRST_ANSWER_B = "src/resources/images/questionsPics/FirstQuestion/FirstAnswerB.png";
    public static final String FIRST_ANSWER_C = "src/resources/images/questionsPics/FirstQuestion/FirstAnswerC.png";
    public static final String FIRST_CORRECT = "src/resources/images/questionsPics/FirstQuestion/FirstAnswerB.png";
    public static final String FIRST_EXPLANATION = "src/resources/images/questionsPics/FirstQuestion/FirstQuestionExplanation.png";


    public static final String SECOND_QUESTION = "src/resources/images/questionsPics/SecondQuestion/SecondQuestion1.png";
    public static final String SECOND_ANSWER_A = "src/resources/images/questionsPics/SecondQuestion/SecondAnswerA.png";
    public static final String SECOND_ANSWER_B = "src/resources/images/questionsPics/SecondQuestion/SecondAnswerB.png";
    public static final String SECOND_ANSWER_C =  "src/resources/images/questionsPics/SecondQuestion/SecondAnswerC.png";
    public static final String SECOND_CORRECT = "src/resources/images/questionsPics/SecondQuestion/SecondAnswerB.png";
    public static final String SECOND_EXPLANATION = "src/resources/images/questionsPics/SecondQuestion/SecondQuestionExplanation.png";

    public static final String THIRD_QUESTION = "src/resources/images/questionsPics/ThirdQuestion/ThirdQuestion1.png";
    public static final String THIRD_ANSWER_A = "src/resources/images/questionsPics/ThirdQuestion/ThirdAnswerA.png";
    public static final String THIRD_ANSWER_B = "src/resources/images/questionsPics/ThirdQuestion/ThirdAnswerB.png";
    public static final String THIRD_ANSWER_C = "src/resources/images/questionsPics/ThirdQuestion/ThirdAnswerC.png";
    public static final String THIRD_CORRECT = "src/resources/images/questionsPics/ThirdQuestion/ThirdAnswerC.png";
    public static final String THIRD_EXPLANATION = "src/resources/images/questionsPics/ThirdQuestion/ThirdQuestionExplanation.png";

    public static final String FOURTH_QUESTION ="src/resources/images/questionsPics/FourthQuestion/FourthQuestion.png";
    public static final String FOURTH_ANSWER_A ="src/resources/images/questionsPics/FourthQuestion/FourthAnswerA.png";
    public static final String FOURTH_ANSWER_B ="src/resources/images/questionsPics/FourthQuestion/FourthAnswerB.png";
    public static final String FOURTH_ANSWER_C ="src/resources/images/questionsPics/FourthQuestion/FourthAnswerC.png";
    public static final String FOURTH_CORRECT ="src/resources/images/questionsPics/FourthQuestion/FourthAnswerA.png";
    public static final String FOURTH_EXPLANATION = "src/resources/images/questionsPics/FourthQuestion/FourthQuestionexplanation.png";

    public static final String FIFTH_QUESTION = "src/resources/images/questionsPics/FifthQuestion/FifthQuestion1.png";
    public static final String FIFTH_ANSWER_A ="src/resources/images/questionsPics/FifthQuestion/FifthAnswerA.png";
    public static final String FIFTH_ANSWER_B ="src/resources/images/questionsPics/FifthQuestion/FifthAnswerB.png";
    public static final String FIFTH_ANSWER_C ="src/resources/images/questionsPics/FifthQuestion/FifthAnswerC.png";
    public static final String FIFTH_CORRECT ="src/resources/images/questionsPics/FifthQuestion/FifthAnswerB.png";
    public static final String FIFTH_EXPLANATION = "src/resources/images/questionsPics/FifthQuestion/FifthQuestionExplanation.png";

    public static final String SIXTH_QUESTION ="src/resources/images/questionsPics/SixthQuestion/SixthQuestion1.png";
    public static final String SIXTH_ANSWER_A ="src/resources/images/questionsPics/SixthQuestion/SixthAnswerA.png";
    public static final String SIXTH_ANSWER_B ="src/resources/images/questionsPics/SixthQuestion/SixthAnswerB.png";
    public static final String SIXTH_ANSWER_C ="src/resources/images/questionsPics/SixthQuestion/SixthAnswerC.png";
    public static final String SIXTH_CORRECT ="src/resources/images/questionsPics/SixthQuestion/SixthAnswerA.png";
    public static final String SIXTH_EXPLANATION = "src/resources/images/questionsPics/SixthQuestion/SixthQuestionExplanation.png";

    public static final String SEVENTH_QUESTION = "src/resources/images/questionsPics/SeventhQuestion/SeventhQuestion1.png";
    public static final String SEVENTH_ANSWER_A ="src/resources/images/questionsPics/SeventhQuestion/SeventhAnswerA.png";
    public static final String SEVENTH_ANSWER_B ="src/resources/images/questionsPics/SeventhQuestion/SeventhAnswerB.png";
    public static final String SEVENTH_ANSWER_C ="src/resources/images/questionsPics/SeventhQuestion/SeventhAnswerC.png";
    public static final String SEVENTH_CORRECT = "src/resources/images/questionsPics/SeventhQuestion/SeventhAnswerC.png";
    public static final String SEVENTH_EXPLANATION= "src/resources/images/questionsPics/SeventhQuestion/SeventhQuestionExplanation.png";


    public static final String EIGHT_QUESTION = "src/resources/images/questionsPics/EightQuestion/EigthQuestion.png";
    public static final String EIGHT_ANSWER_A ="src/resources/images/questionsPics/EightQuestion/EightAnswerA.png";
    public static final String EIGHT_ANSWER_B = "src/resources/images/questionsPics/EightQuestion/EightAnswerB.png";
    public static final String EIGHT_ANSWER_C ="src/resources/images/questionsPics/EightQuestion/EightAnswerC.png";
    public static final String EIGHT_CORRECT ="src/resources/images/questionsPics/EightQuestion/EightAnswerA.png";
    public static final String EIGHT_EXPLANATION = "src/resources/images/questionsPics/EightQuestion/EightQuestionExplanation.png" ;


    public static final String NINTH_QUESTION = "src/resources/images/questionsPics/NinthQuestion/NinthQuestion1.png";
    public static final String NINTH_ANSWER_A ="src/resources/images/questionsPics/NinthQuestion/NinthAnswerA.png";
    public static final String NINTH_ANSWER_B ="src/resources/images/questionsPics/NinthQuestion/NinthAnswerB.png";
    public static final String NINTH_ANSWER_C ="src/resources/images/questionsPics/NinthQuestion/NinthAnswerC.png";
    public static final String NINTH_CORRECT ="src/resources/images/questionsPics/NinthQuestion/NinthAnswerC.png";
    public static final String NINTH_EXPLANATION = "src/resources/images/questionsPics/NinthQuestion/NinthQuestionExplanation.png";

    public static final String TENTH_QUESTION ="src/resources/images/questionsPics/TenthQuestion/TenthQuestion.png";
    public static final String TENTH_ANSWER_A ="src/resources/images/questionsPics/TenthQuestion/TenthAnswerA.png";
    public static final String TENTH_ANSWER_B ="src/resources/images/questionsPics/TenthQuestion/TenthAnswerB.png";
    public static final String TENTH_ANSWER_C ="src/resources/images/questionsPics/TenthQuestion/TenthAnswerC.png";
    public static final String TENTH_CORRECT ="src/resources/images/questionsPics/TenthQuestion/TenthAnswerB.png";
    public static final String TENTH_EXPLANATION = "src/resources/images/questionsPics/TenthQuestion/TenthQuestionExplanation.png";



}
