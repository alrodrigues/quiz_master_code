package utils;

import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;

public enum Key {

    ANSWER_A(KeyboardEvent.KEY_A),
    ANSWER_B(KeyboardEvent.KEY_B),
    ANSWER_C (KeyboardEvent.KEY_C),
    SPACE (KeyboardEvent.KEY_SPACE);

    private int keyCode;

    Key(int keyCode){
        this.keyCode = keyCode;
    }

    public int getKeyCode() {
        return keyCode;
    }

    public void setKeyCode(int keyCode) {
        this.keyCode = keyCode;
    }
}
