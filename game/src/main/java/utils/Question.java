package utils;

import Canvas.Imagens;

public enum Question {

    FIRST(new String[]{Imagens.FIRST_QUESTION, Imagens.FIRST_ANSWER_A, Imagens.FIRST_ANSWER_B, Imagens.FIRST_ANSWER_C, Imagens.FIRST_CORRECT, Imagens.FIRST_EXPLANATION}),
    SECOND(new String[]{Imagens.SECOND_QUESTION, Imagens.SECOND_ANSWER_A, Imagens.SECOND_ANSWER_B, Imagens.SECOND_ANSWER_C, Imagens.SECOND_CORRECT , Imagens.SECOND_EXPLANATION }),
    THIRD(new String[]{Imagens.THIRD_QUESTION, Imagens.THIRD_ANSWER_A, Imagens.THIRD_ANSWER_B, Imagens.THIRD_ANSWER_C, Imagens.THIRD_CORRECT , Imagens.THIRD_EXPLANATION }),
    FOURTH(new String[]{Imagens.FOURTH_QUESTION, Imagens.FOURTH_ANSWER_A, Imagens.FOURTH_ANSWER_B, Imagens.FOURTH_ANSWER_C, Imagens.FOURTH_CORRECT , Imagens.FOURTH_EXPLANATION }),
    FIFTH(new String[]{Imagens.FIFTH_QUESTION,Imagens.FIFTH_ANSWER_A , Imagens.FIFTH_ANSWER_B, Imagens.FIFTH_ANSWER_C, Imagens.FIFTH_CORRECT , Imagens.FIFTH_EXPLANATION}),
    SIXTH(new String[]{Imagens.SIXTH_QUESTION,Imagens.SIXTH_ANSWER_A , Imagens.SIXTH_ANSWER_B, Imagens.SIXTH_ANSWER_C, Imagens.SIXTH_CORRECT , Imagens.SIXTH_EXPLANATION}),
    SEVENTH(new String[]{Imagens.SEVENTH_QUESTION, Imagens.SEVENTH_ANSWER_A , Imagens.SEVENTH_ANSWER_B , Imagens.SEVENTH_ANSWER_C , Imagens.SEVENTH_CORRECT , Imagens.SEVENTH_EXPLANATION }),
    EIGHT(new String[]{Imagens.EIGHT_QUESTION, Imagens.EIGHT_ANSWER_A, Imagens.EIGHT_ANSWER_B, Imagens.EIGHT_ANSWER_C,Imagens.EIGHT_CORRECT, Imagens.EIGHT_EXPLANATION}),
    NINTH(new String[]{Imagens.NINTH_QUESTION, Imagens.NINTH_ANSWER_A, Imagens.NINTH_ANSWER_B, Imagens.NINTH_ANSWER_C, Imagens.NINTH_CORRECT, Imagens.NINTH_EXPLANATION}),
    TENTH(new String[]{Imagens.TENTH_QUESTION, Imagens.TENTH_ANSWER_A , Imagens.TENTH_ANSWER_B , Imagens.TENTH_ANSWER_C, Imagens.TENTH_CORRECT , Imagens.TENTH_EXPLANATION});

    private String[] questionArray;

    Question(String[] questionArray) {
        this.questionArray = questionArray;
    }

    public String[] getQuestionArray() {
        return this.questionArray;
    }


}


