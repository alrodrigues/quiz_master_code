package utils;

import Canvas.Animations;
import Canvas.Imagens;
import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class KeyboardController implements KeyboardHandler {

    private Game game;
    private String playerAnswer;
    private KeyboardEvent[] keys;
    private Animations animations = new Animations();


    public KeyboardController(Game game){
        this.game = game;
        initKeyboard();
    }

    public void initKeyboard() {
        Keyboard keyboard = new Keyboard(this);
        keys = new KeyboardEvent[Key.values().length];

        for (int i = 0; i < Key.values().length; i++) {
            keys[i] = subscribe(keyboard, Key.values()[i], KeyboardEventType.KEY_RELEASED);
        }

    }

    private KeyboardEvent subscribe(Keyboard keyboard, Key key, KeyboardEventType type) {
        KeyboardEvent event = new KeyboardEvent();
        event.setKey(key.getKeyCode());
        event.setKeyboardEventType(type);
        keyboard.addEventListener(event);
        return event;
    }


    public void keyReleased(KeyboardEvent keyboardEvent) {

        switch (keyboardEvent.getKey()){
            case KeyboardEvent.KEY_SPACE:
                game.setStarted(false);
                qualquerCoisa();
               //System.out.println(game.isEndRound()+ "TOP");
                game.setEndRound(true);
                //System.out.println(game.isEndRound()+ "Below");


               break;
        }

    }

    public void qualquerCoisa(){
        if(game.isGameOver() || game.isWin()){
            System.exit(0);

        }
      if(!game.createQuestion()){
            setEventTypeInKeys(KeyboardEventType.KEY_RELEASED);
        }else {
            setEventTypeInKeys(KeyboardEventType.KEY_PRESSED);
        }


    }


    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {

        switch (keyboardEvent.getKey()){

            case KeyboardEvent.KEY_A:
                playerAnswer = game.getCurrentQuestion().getQuestionArray()[1];
                game.createBackground(game.checkAnswer(playerAnswer));
                setEventTypeInKeys(KeyboardEventType.KEY_RELEASED);
                game.setEndRound(false);

                break;
            case KeyboardEvent.KEY_B:
                playerAnswer = game.getCurrentQuestion().getQuestionArray()[2];
                game.createBackground(game.checkAnswer(playerAnswer));
                setEventTypeInKeys(KeyboardEventType.KEY_RELEASED);
                game.setEndRound(false);
                break;
            case KeyboardEvent.KEY_C:
                playerAnswer = game.getCurrentQuestion().getQuestionArray()[3];
                game.createBackground(game.checkAnswer(playerAnswer));
                setEventTypeInKeys(KeyboardEventType.KEY_RELEASED);
                game.setEndRound(false);
                break;
        }


    }

    public void setEventTypeInKeys(KeyboardEventType event) {
        for (int i = 0; i < Key.values().length; i++) {
            keys[i].setKeyboardEventType(event);
        }
    }

    public String getPlayerAnswer() {
        return playerAnswer;
    }


}
