package utils;

import Canvas.Animations;
import Canvas.Drawings;
import Canvas.Imagens;
import Sound.PlayingSound;
import org.academiadecodigo.simplegraphics.pictures.Picture;


public class Game {

    private boolean gameOver;
    private boolean win;
    private int lives = 3;
    private int correctAnswers;
    private boolean playingSound = true;
    private boolean started;
    private static boolean isAnimating;
    private Question currentQuestion;
    private Drawings drawings;
    private Animations animations;
    private boolean endRound = false;

    public Game() {
        this.gameOver = false;
        this.win = false;
        this.started = true;
        this.drawings = new Drawings();
        this.animations = new Animations();
    }

    public void initGame() {
        drawings.drawBackground(Imagens.START_BACKGROUND);
        Picture smallPressSpace = drawings.drawPressSpaceStart();

        Questions.createOrderQuestions();
        Questions.createQuestions();
        PlayingSound.playBackground();

        while (started) {
            animations.flashButton(smallPressSpace);
        }


        while (true) {
           System.out.println("while sout");
            if (endRound) {
                if (Questions.getQuestion1().getX() != 90) {
                    animations.moveTextFromLeft(Questions.getQuestion1());
                    continue;
                }
                if (Questions.getFirstAnswer().getY() != 110) {
                    animations.moveTextFromTop(Questions.getFirstAnswer());
                    continue;
                }
                if (Questions.getSecondAnswer().getY() != 330) {
                    animations.moveTextFromTop(Questions.getSecondAnswer());
                    continue;
                }
                if (Questions.getThirdAnswer().getY() != 540) {
                    animations.moveTextFromTop(Questions.getThirdAnswer());
                    continue;
                }

            }
        }
    }



    public static void setIsAnimatingFalse(){
        isAnimating=false;
    }

    public static void setIsAnimatingTrue(){
        isAnimating=true;
    }




    public void createBackground(boolean checkAnswer) {
        PlayingSound.stopBackground();
        playingSound = false;

        drawings.drawText(Questions.getTextExplanation());
        drawings.drawText(Questions.getTextExplanation());
        drawings.drawPressSpaceNext();

        if (checkAnswer) {
            PlayingSound.playCorrectAnswer();
            correctAnswers++;
            return;
        }

        PlayingSound.playWrongAnswer();
        drawings.hideTextLives();
        lives--;
        drawings.drawLives(lives);

    }


    public boolean createQuestion() {

        if (checkEndGame()) return false;

        if (!playingSound) {
            playingSound = true;
            PlayingSound.playBackground();
        }

        drawings.drawBackground(Imagens.QUESTIONS_BACKGROUND);
        currentQuestion = Questions.generateQuestion();
        drawings.drawLives(lives);
        return true;
    }

    public boolean checkAnswer(String playerAnswer) {
        return playerAnswer.equals(Questions.getRightAnswer(currentQuestion));
    }

    public boolean checkEndGame() {

        gameOver = (lives == 0);
        win = Questions.getCurrentIndex() == Question.values().length;

        if (gameOver) {
            drawings.drawBackground(Imagens.GAME_OVER_BACKGROUND);
            drawings.drawPressSpaceEndGame();
            return true;
        }

        if (win) {
            PlayingSound.playVictory();
            drawings.drawBackground(Imagens.WIN_BACKGROUND);
            drawings.drawCorrectAnswers(correctAnswers);
            return true;
        }
        return false;
    }


    public Question getCurrentQuestion() {
        return currentQuestion;
    }

    public boolean isGameOver() {
        return gameOver;
    }

    public boolean isWin() {
        return win;
    }

    public void setStarted(boolean started) {
        this.started = started;
    }

    public void setEndRound(boolean endRound) {
        this.endRound = endRound;
    }

    public boolean isEndRound() {
        return endRound;
    }
}




