package utils;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Text;
import org.academiadecodigo.simplegraphics.pictures.Picture;

import java.util.Random;

public class Questions {

    private static int totalNumQuestions = Question.values().length;
    private static int currentIndex = 0;
    private static int[] questionIndexOrder;

    private static Picture textQuestion1;
    private static Picture firstAnswer;
    private static Picture secondAnswer;
    private static Picture thirdAnswer;
    private static Picture textExplanation;
    private static Text explanation;

    public static Question generateQuestion() {

        if (currentIndex == totalNumQuestions) {
            currentIndex = 0;
            createQuestions();
        }
        Question[] questions = Question.values();
        Question currentQuestion = questions[questionIndexOrder[currentIndex]];
        drawQuestion(currentQuestion);
        currentIndex++;
        return currentQuestion;
    }



    public static void createOrderQuestions() {

        questionIndexOrder = new int[totalNumQuestions];
        for (int i = 0; i < totalNumQuestions; i++) {
            questionIndexOrder[i] = i;
        }
    }


    public static int getCurrentIndex() {
        return currentIndex;
    }


    public static void createQuestions() {

        Random randomGenerator = new Random();
        int randomIndex;
        int randomValue;

        for (int i = 0; i < totalNumQuestions; i++) {
            randomIndex = randomGenerator.nextInt(totalNumQuestions);

            randomValue = questionIndexOrder[randomIndex];
            questionIndexOrder[randomIndex] = questionIndexOrder[i];
            questionIndexOrder[i] = randomValue;
        }

    }

    public static String getRightAnswer(Question question) {

        return question.getQuestionArray()[4];

    }

    private static void drawQuestion(Question currentQuestion){

        explanation = new Text(150, 420, "");
        explanation.setColor(Color.MAGENTA);

        textExplanation = new Picture(90, 370, currentQuestion.getQuestionArray()[5]);

        textQuestion1 = new Picture(-400, 160, currentQuestion.getQuestionArray()[0]);
        textQuestion1.draw();

        firstAnswer = new Picture(660, -200, currentQuestion.getQuestionArray()[1]);
        firstAnswer.draw();

        secondAnswer = new Picture(660, -200, currentQuestion.getQuestionArray()[2]);
        secondAnswer.draw();

        thirdAnswer = new Picture(660, -200, currentQuestion.getQuestionArray()[3]);
        thirdAnswer.draw();

        Game.setIsAnimatingTrue();
    }

    public static Picture getTextExplanation() {
        return textExplanation;
    }

    public static Picture getQuestion1(){
        return  textQuestion1;
    }

    public static Picture getFirstAnswer() {
        return firstAnswer;
    }

    public static Picture getSecondAnswer() {
        return secondAnswer;
    }

    public static Picture getThirdAnswer() {
        return thirdAnswer;
    }

}